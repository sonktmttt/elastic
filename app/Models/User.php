<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \Eloquent
{
    use Notifiable, ElasticquentTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The elasticsearch settings.
     *
     * @var array
     */
    protected $indexSettings = [
        'analysis' => [
            'filter' => [
                // filter synonym
                'synonym_filter' => [
                    'type' => 'synonym_graph',
                    'format' => 'wordnet',
                    'synonyms_path' => 'analysis/wn_s.pl'
                ],
                // filter autocomplete
                // "autocomplete_filter" => [
                //     "type" =>     "edge_ngram",
                //     "min_gram" => 2,
                //     "max_gram" => 20,
                //     "token_chars"=> [
                //         "letter"
                //     ]
                // ]
            ],
            'analyzer' => [
                // analyzer synonym
                'synonym' => [
                    'tokenizer' => 'whitespace',
                    'filter' => ['synonym_filter']
                ],
                // analyzer autocomplete
                // "autocomplete" => [
                //     "type" => "custom",
                //     "tokenizer" => "whitespace",
                //     "filter" => [
                //         "autocomplete_filter"
                //     ]
                // ]
            ]

        ]
    ];

    protected $mappingProperties = [
        'name'=> [
            'type'=> 'text',
            'analyzer'=> 'synonym'
        ],
        'profile'=> [
            'type'=> 'text',
            'analyzer'=> 'synonym'
        ],
        'email'=> [
            'type'=> 'text',
            'analyzer'=> 'synonym'
        ],
        'address'=> [
            'type'=> 'text',
            'analyzer'=> 'synonym'
        ]
    ];

    /**
     * Scope a query of name
     *
     * @param      builder  $query
     * @param      int      $name
     *
     * @return     builder
     */
    public function scopeOfName($query, $name)
    {
        if ($name == '') {
            return $query;
        }

        // search like
        return $query->where($this->getTable() . '.name', 'LIKE', '%' . $name . '%');

        // full text search
        return $query->whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $name);
    }

    /**
     * Scope a query of address
     *
     * @param      builder  $query
     * @param      int      $address
     *
     * @return     builder
     */
    public function scopeOfAddress($query, $address)
    {
        if ($address == '') {
            return $query;
        }

        // search like
        return $query->where($this->getTable() . '.address', 'LIKE', '%' . $address . '%');
    }
}
