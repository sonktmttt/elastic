<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as AppController;
use \App\Models as Database;

class UsersController extends AppController
{
    /**
     * List user
     *
     * /users
     */
    public function index(Request $request)
    {
        $key = $request->get('name');

        // search in mysql
        if (!isset($key)) {
            $users = Database\User::paginate(200);
        } else {
            // $users = Database\User::ofName($key)
            //     ->paginate(200);
            // return view('users.index', compact('users'));

            // search in elastic
            // Query Based Search
            $highlight = [
                "fields" => [
                    "name" => [
                        "require_field_match" => false,
                        "fragment_size" => 150,
                        "number_of_fragments" => 1,
                        "no_match_size" => 0,
                        "pre_tags" => ["<b>"],
                        "post_tags" => ["</b>"],
                    ],
                    "address" => [
                        "require_field_match" => false,
                        "fragment_size" => 150,
                        "number_of_fragments" => 1,
                        "no_match_size" => 0,
                        "pre_tags" => ["<b>"],
                        "post_tags" => ["</b>"],
                    ],
                    "profile" => [
                        "require_field_match" => false,
                        "fragment_size" => 150,
                        "number_of_fragments" => 1,
                        "no_match_size" => 0,
                        "pre_tags" => ["<b>"],
                        "post_tags" => ["</b>"],
                    ],
                    "email" => [
                        "require_field_match" => false,
                        "fragment_size" => 150,
                        "number_of_fragments" => 1,
                        "no_match_size" => 0,
                        "pre_tags" => ["<b>"],
                        "post_tags" => ["</b>"],
                    ]
                ]
            ];

            // query basic
            $query = [
                "match" => [
                    "name" => [
                        "query" => $key,
                        "operator" => "and"
                    ]
                ]
            ];

            //query bool
            // $query = [
            //     "bool" => [
            //         "should" => [
            //             [
            //                 "match_phrase" => [
            //                     "profile" => $key
            //                 ]
            //             ],
            //             [
            //                 "match_phrase" => [
            //                     "name" => $key
            //                 ]
            //             ],
            //             [
            //                 "match_phrase" => [
            //                     "address" => $key
            //                 ]
            //             ]
            //         ]
            //     ]
            // ];

            $users = Database\User::searchByQuery(
                $query,
                $aggregations = null,
                $sourceFields = null,
                $limit = 200,
                $offset = 0,
                $sort = null,
                $highlight
            );
        }

        return view('users.index', compact('users'));
    }

    /**
     * Create new user
     *
     * GET users/create
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Edit user
     *
     * GET users/{id}/edit
     *
     * @param Database\User $user
     */
    public function edit(Database\User $user)
    {
        return view('users.edit', compact(
            'user'
        ));
    }

    public function addIndex()
    {
        Database\User::createIndex($shards = null, $replicas = null);
        Database\User::putMapping($ignoreConflicts = true);

        $numberUsers = Database\User::count();
        $i = 0;
        while ($i * 99999 < $numberUsers) {
            $users = Database\User::whereBetween('id', [$i * 99999, ($i + 1) * 99999 - 1])->get();
            $users->addToIndex();
            $i++;
        };
    }
}
