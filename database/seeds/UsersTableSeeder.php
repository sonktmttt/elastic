<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::truncate();
        $faker = \Faker\Factory::create('en_US');

        for ($i = 0; $i < 1000000; $i++) {
            App\Models\User::create([
                'name' => $faker->name,
                'email' => $faker->safeEmail,
                'address' => $faker->address,
                'profile' => $faker->realText,
                'password' => '$2y$10$V9arhn9ZFy3UbJaNYjkgjekvJXIXTlZ8JAmopyhogWEvpP8./TPHK', // secret
                'remember_token' => '',
            ]);
        }
    }
}
