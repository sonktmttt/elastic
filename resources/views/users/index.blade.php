@extends('layouts.main')
@section('title', __('Quản lý người dùng'))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="btn-link panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">{{ __('▼　Danh sách') }}</h3>
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{
                Form::open([
                    'class' => 'form-horizontal',
                    'role' => 'form',
                    'method' => 'get'
                ])
            }}
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        {{
                            Form::text('name', '', ['class' => 'form-control'])
                        }}
                        <span class="input-group-btn">
                        {{
                            Form::submit( __('Tìm kiếm'), ['class' => 'btn btn-default'])
                        }}
                        </span>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-left">
            <p class="pagination">
                {{
                    Html::link(route('users.create'), __('Thêm mới'), ['class' => 'btn btn-default'])
                }}
            </p>
        </div>
        <div class="col-md-4 text-left">
            <p>
                {{-- {{ __('Tổng số: ') . $users->totalHits()  }} --}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ __('STT') }}</th>
                        <th>{{ __('Tên') }}</th>
                        <th>{{ __('Địa chỉ') }}</th>
                        <th>{{ __('Thông tin') }}</th>
                        <th>{{ __('Địa chỉ email') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{!! html_entity_decode($user->name) !!}</td>
                        <td>{!! html_entity_decode($user->address) !!}}</td>
                        <td>{!! html_entity_decode($user->profile) !!}</td>
                        <td>{!! html_entity_decode($user->email) !!}</td>
                        <td>
                            <p class="text-right">
                                {{ Html::link(route('users.edit', $user), __('Sửa')) }}
                            </p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 text-left">
            <p class="pagination">
                {{
                    Html::link(route('users.create'), __('Thêm mới'), ['class' => 'btn btn-default'])
                }}
            </p>
        </div>
        @if ($users->count() > 0)
            <div class="col-md-8 text-right">
                {{-- @include('elements.pagination', ['records' => $users]) --}}
            </div>
        @endif
    </div>
@stop