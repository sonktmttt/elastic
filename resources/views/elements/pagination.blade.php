@if ($records->lastPage() != 1)
    {{ $records->appends(app('request')->all())->links() }}
@endif