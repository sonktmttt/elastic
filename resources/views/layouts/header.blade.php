<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">{{ __('Toggle navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{ Html::link(url('/'), __('Next Front System / ADMIN'), ['class' => 'navbar-brand']) }}
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ __('メニュー') }}<i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="divider"></li>
                        <li>
                            {{ Html::link(route('users.index'), __('Danh sách người dùng')) }}
                        </li>
                    </ul>
                </li>
                <li class="divider hidden-xs">
                    {{ Html::link('#', __('|', ['onfocus'=>'javascript:blur();'])) }}
                </li>
            </ul>
        </div>
    </div>
</div>